<?php

namespace part1\intFace;

interface IFormat
{
    /**
     * Get correctly format
     *
     * @param $string
     * @return string
     */
    public function getFormat($string);
}