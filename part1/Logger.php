<?php

namespace part1;

use part1\intFace\IDeliver;
use part1\intFace\IFormat;

class Logger
{
    private $format;
    private $delivery;

    public function __construct(IFormat $format, IDeliver $delivery)
    {
        $this->format = $format;
        $this->delivery = $delivery;
    }

    public function log($string)
    {
        $this->delivery->getDeliver($this->format->getFormat($string));
    }
}