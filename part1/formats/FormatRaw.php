<?php

namespace part1\formats;


use part1\intFace\IFormat;

class FormatRaw implements IFormat
{
    /**
     * Get correctly format
     *
     * @param $string
     * @return string
     */
    public function getFormat($string)
    {
        return $string;
    }
}