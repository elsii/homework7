<?php

namespace part1\formats;

use part1\intFace\IFormat;

class FormatWithDateAndDetails implements IFormat
{
    /**
     * Get correctly format
     *
     * @param $string
     * @return string
     */
    public function getFormat($string)
    {
        return date('Y-m-d H:i:s') . $string . ' - With some details';
    }
}