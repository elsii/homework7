<?php

namespace part1\delivers;

use part1\intFace\IDeliver;

class DeliverBySMS implements IDeliver
{
    public function getDeliver($format)
    {
        echo "Вывод формата ({$format}) в смс";
    }
}