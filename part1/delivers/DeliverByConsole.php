<?php

namespace part1\delivers;

use part1\intFace\IDeliver;

class DeliverByConsole implements IDeliver
{
    public function getDeliver($format)
    {
        echo "Вывод формата ({$format}) в консоль";
    }
}