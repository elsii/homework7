<?php

include_once 'vendor/autoload.php';

use part1\formats\FormatRaw;
use part1\delivers\DeliverBySMS;
use part1\Logger;

$logger = new Logger(new FormatRaw(), new DeliverBySMS());
$logger->log('Emergency error! Please fix me!');