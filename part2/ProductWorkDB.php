<?php

namespace part2;

class ProductWorkDB
{
    public function save()
    {
        print 'save product';
    }

    public function update()
    {
        print 'update product';
    }

    public function delete()
    {
        print 'delete product';
    }
}