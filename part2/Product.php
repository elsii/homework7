<?php

namespace part2;

class Product
{
    private $name;

    public function get()
    {
        return $this->name;
    }

    public function set($value)
    {
        $this->name = $value;
    }
}